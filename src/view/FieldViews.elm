module FieldViews exposing (..)

import Model exposing (Msg)
import FieldConfig exposing (..)
import Html exposing (..)
import Html.Attributes exposing (class)


fieldLabel : Field -> Html Msg
fieldLabel field =
    label [] [ text field.key ]


unknownFieldView : Field -> Html Msg
unknownFieldView field =
    div [ class "field", class "unknown-field" ]
        [ text ("Unkown Field: " ++ field.key ++ " Type: " ++ (toString field.fieldType)) ]


textFieldView : Field -> Html Msg
textFieldView field =
    div [ class "field", class "text-field" ]
        [ fieldLabel field
        , input [] []
        ]
