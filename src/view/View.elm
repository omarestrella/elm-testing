module View exposing (view)

import Model exposing (Model, Msg)
import User exposing (User)
import FieldConfig exposing (..)
import FieldViews exposing (..)
import Html exposing (Html, text, div, span, button, br, h2)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)


fieldView : Field -> Html Msg
fieldView field =
    div [ class "field", class field.key ]
        [ (case field.fieldType of
            Text ->
                (textFieldView field)

            _ ->
                (unknownFieldView field)
          )
        ]


sectionView : Section -> Html Msg
sectionView section =
    div [ class "section", class section.key ]
        [ h2 [] [ text section.key ]
        , div [ class "fields" ] (List.map fieldView section.fieldList)
        ]


fieldConfigView : Maybe User -> Html Msg
fieldConfigView user =
    case user of
        Just user ->
            div [ class "field-config-view" ]
                (List.map sectionView user.fieldConfigs.person.sections)

        Nothing ->
            div [] [ text "Fetch user to display field config" ]


view : Model -> Html Msg
view model =
    div [ class "main" ]
        [ div [ class "actions" ]
            [ span [ class "message" ] [ text model.message ]
            , button [ onClick Model.FetchData ] [ text "Get User" ]
            ]
        , fieldConfigView model.user
        ]
