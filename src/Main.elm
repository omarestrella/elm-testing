module Main exposing (..)

import Model exposing (..)
import View
import Update
import Subscriptions
import Html exposing (..)


main : Program Never Model Msg
main =
    Html.program
        { view = View.view
        , init = Model.init
        , update = Update.update
        , subscriptions = Subscriptions.subscriptions
        }
