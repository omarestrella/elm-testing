module Update exposing (update)

import Model exposing (Model, Msg)
import User exposing (User)
import UserDecoder


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Model.NoOp ->
            model ! []

        Model.FetchData ->
            { model | message = "Fetching data!" } ! [ UserDecoder.request ]

        Model.FetchCompleted (Ok user) ->
            { model | user = Just user, message = "Fetch success!" } ! []

        Model.FetchCompleted (Err err) ->
            let
                _ =
                    Debug.log "FetchCompleted error:" err
            in
                { model | message = "Fetching error :(" } ! []
