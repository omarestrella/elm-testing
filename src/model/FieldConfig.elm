module FieldConfig exposing (..)


type FieldType
    = Text
    | MultilineText
    | Boolean
    | Select
    | Email
    | Image
    | Weblink
    | Group
    | Date
    | History
    | Address
    | Location
    | Phone
    | Unknown


type alias Field =
    { customLabels : Bool
    , key : String
    , repeatable : Bool
    , fieldType : FieldType
    , required : Bool

    -- , params : List String
    }


type alias Section =
    { fieldList : List Field
    , instructionText : String
    , key : String
    , state : String
    }


type alias FieldConfig =
    { id : String
    , state : String
    , entityType : String
    , version : String
    , schemaVersion : String
    , sections : List Section
    }
