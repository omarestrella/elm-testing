module UserDecoder exposing (request, decoder)

import Http
import Json.Decode as Decode
import Json.Decode.Pipeline exposing (..)
import Constants
import FieldConfigDecoder exposing (fieldConfigDecoder)
import Model exposing (Msg)
import User exposing (User, UserFieldConfigs)


fetch : String -> Http.Request User
fetch url =
    let
        headers =
            [ (Http.header "Authorization" ("Bearer " ++ Constants.authToken))
            ]
    in
        Http.request
            { method = "GET"
            , headers = headers
            , url = url
            , body = Http.emptyBody
            , expect = Http.expectJson decoder
            , timeout = Nothing
            , withCredentials = False
            }


request : Cmd Msg
request =
    let
        requestUrl =
            Constants.baseUrl
                ++ "/api/v2/users/me"
                ++ ("?expand=fieldconfigs")
    in
        Http.send Model.FetchCompleted (fetch requestUrl)


userFieldConfigsDecoder : Decode.Decoder UserFieldConfigs
userFieldConfigsDecoder =
    decode User.UserFieldConfigs
        |> required "org" (fieldConfigDecoder)
        |> required "person" (fieldConfigDecoder)
        |> required "group" (fieldConfigDecoder)


decoder : Decode.Decoder User
decoder =
    decode User
        |> required "id" Decode.string
        |> required "name" Decode.string
        |> required "email" Decode.string
        |> required "state" Decode.string
        |> required "username" Decode.string
        |> required "version" Decode.int
        |> required "fieldConfigs" (userFieldConfigsDecoder)
