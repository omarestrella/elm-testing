module User exposing (..)

import FieldConfig exposing (..)


type alias UserFieldConfigs =
    { org : FieldConfig
    , person : FieldConfig
    , group : FieldConfig
    }


type alias User =
    { id : String
    , name : String
    , email : String
    , state : String
    , username : String
    , version : Int
    , fieldConfigs : UserFieldConfigs
    }
