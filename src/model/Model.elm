module Model exposing (..)

import User exposing (User)
import Http


type alias Model =
    { user : Maybe User
    , message : String
    }


type Msg
    = NoOp
    | FetchData
    | FetchCompleted (Result Http.Error User)


init : ( Model, Cmd Msg )
init =
    { user = Nothing, message = "Hello!" } ! []
