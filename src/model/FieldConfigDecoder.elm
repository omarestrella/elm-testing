module FieldConfigDecoder exposing (..)

import Json.Decode as Decode exposing (succeed)
import Json.Decode.Pipeline exposing (..)
import FieldConfig exposing (..)


typeDecoder : String -> Decode.Decoder FieldType
typeDecoder type_ =
    case type_ of
        "text" ->
            succeed Text

        "multiline-text" ->
            succeed MultilineText

        "boolean" ->
            succeed Boolean

        "select" ->
            succeed Select

        "email" ->
            succeed Email

        "image" ->
            succeed Image

        "weblink" ->
            succeed Weblink

        "group" ->
            succeed Group

        "date" ->
            succeed Date

        "history" ->
            succeed History

        "address" ->
            succeed Address

        "location" ->
            succeed Location

        "phone" ->
            succeed Phone

        _ ->
            let
                _ =
                    Debug.log "Found unknown field type: " type_
            in
                Decode.succeed Unknown



-- This helped for field type decoding:
-- https://github.com/NoRedInk/elm-decode-pipeline/issues/25


fieldDecoder : Decode.Decoder Field
fieldDecoder =
    decode Field
        |> required "customLabels" Decode.bool
        |> required "key" Decode.string
        |> required "required" Decode.bool
        |> required "type" (Decode.string |> Decode.andThen typeDecoder)
        |> required "repeatable" Decode.bool


sectionDecoder : Decode.Decoder Section
sectionDecoder =
    decode Section
        |> required "fieldList" (Decode.list fieldDecoder)
        |> required "instructionText" Decode.string
        |> required "key" Decode.string
        |> required "state" Decode.string


fieldConfigDecoder : Decode.Decoder FieldConfig
fieldConfigDecoder =
    decode FieldConfig
        |> required "id" Decode.string
        |> required "state" Decode.string
        |> required "entityType" Decode.string
        |> required "version" Decode.string
        |> required "schemaVersion" Decode.string
        |> required "sections" (Decode.list sectionDecoder)
